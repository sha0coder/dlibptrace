#include <stdio.h>



#define FN_START(fn) (fn+4)
#define FN_END(fn) (fn##_end-3)
#define FN_SZ(fn) (FN_END(fn)-FN_START(fn))



void test() {
	printf("funca\n");
}

void test_end() {}



int main() {
	printf("sz: %d\n",FN_SZ(test));
}
