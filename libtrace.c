// Process trace helper jesus.olmos@blueliv.com
// http://www.phrack.org/issues.html?issue=59&id=8&mode=txt
// http://www.linuxjournal.com/article/6100

#include <sys/ptrace.h>
#include <sys/types.h>
#include <sys/reg.h>
#include <sys/wait.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <stdio.h>
#include <errno.h>

#include "libtrace.h"

static sigset_t empty_set, blocked_set;

char getProcessStatus(pid_t pid) {
	/*
       D   uninterruptible sleep (usually IO)
       R   runnable (on run queue)
       S   sleeping
       T   traced or stopped
       t   
       Z   a defunct ("zombie") process

       For BSD formats and when the "stat" keyword is used, additional letters may be displayed:
       W   has no resident pages
       <   high-priority process
       N   low-priority task
       L   has pages locked into memory (for real-time and custom IO)
	*/

	char name[255];
	FILE *fp;
	char *line = NULL;
	size_t len = 0;
	char ret = '-';

	snprintf(name,254,"/proc/%d/status", pid);
	fp = fopen(name,"r");
	if (fp == NULL) {
		printf("can't open file\n");
		exit(EXIT_FAILURE);
    }

	while (getline(&line, &len, fp) != -1) {
		if (strncmp(line, "State:", 6) == 0) {
			ret = line[7];
			//printf(line);
			break;
		}
	}

	fclose(fp);
	free(line);
	return ret;
}

char wait_cont(pid_t pid) {
	int stat,ret;
	unsigned long __signum;

	while (1) {
		
		// get non EINTR status
		do {
			ret = waitpid(pid, &stat, 0);
		} while (ret == -1 && errno == EINTR);

		// process ended
		if (WIFEXITED(stat) || WIFSIGNALED(stat))
			return 0;

		// got a stop signal
		if (WIFSTOPPED(stat)) {
			switch (WSTOPSIG(stat)) {
				case -1:
					return 0;

				case SIGCONT:
				case SIGSTOP:
					return 1;

				default:
					__signum = (unsigned long)WSTOPSIG(stat);
					if (ptrace(PTRACE_CONT, pid, NULL, (void *)__signum) == -1)
						return -1;
			}
		}
	}	
}

/*
void cont_syscall(pid_t pid) {
	ptrace(PTRACE_PEEKUSER, pid, 4 * ORIG_EAX, NULL);
}*/


void cont_syscall(pid_t pid) {
	if (ptrace(PTRACE_SYSCALL, pid, NULL, NULL) > 0)
		perror("PTRACE_SYSCALL");
}


int launch(const char *file_name) {
	int pid = 0;
	int stat;
	
	if ((pid = fork())) {
		wait(&stat);
	} else {
		ptrace(PTRACE_TRACEME, 0, NULL, NULL);
		execl(file_name, file_name, NULL);
		perror("execl failed");
	}
	
	return pid;
}
 
char ptrace_attach(pid_t pid) {
	char pstat;
	int sigstat;

	//sigprocmask(SIG_BLOCK, &blocked_set, NULL);

	#ifdef USE_SEIZE
	
	printf("using seize ..\n");
	
	if (ptrace(PTRACE_SEIZE, pid, 0, 0) < 0)
		perror("ptrace_seize");
	
	if (ptrace(PTRACE_INTERRUPT, pid, 0, 0) < 0)
		perror("ptrace_interrupt");
	
	#else
	
	//kill(pid,SIGSTOP);

	if (ptrace(PTRACE_ATTACH, pid, 0, 0) < 0) {
		printf("can't attach to %d\n",pid);
		return 0;
	}

	/*
		tracing stop (t)  --- PTRACE_CONT --->  trace suspended (T)
		suspended ----- detach+SIGSTOP+attach --> tracing stop
	*/

	waitpid(pid, &sigstat, 0);

	pstat = getProcessStatus(pid);
	//if (pstat == 't') {
	//	cont(pid);
		//wait_cont(pid);
	//}
	if (pstat != 't' && pstat != 'T') {
		printf("can't stop process\n");
		return 0;
	}


	cont_syscall(pid);
	waitpid(pid, &sigstat, 0);

	printf("before ptrace syscall status: %c\n",getProcessStatus(pid));
	


	
	//sleep(3);


	//wait_cont(pid);

	//contSyscall(pid);
	
	#endif
	
	
	
	return 1;
}


char ptrace_detach(pid_t pid) {
	//kill(pid,SIGSTOP);
	if (ptrace(PTRACE_DETACH,pid,NULL,NULL) < 0){
    		printf ("can't dettach pid %d\n", pid); 
        	_exit (1);
	}
	stat(pid);
	return 0;
}

char get_regs(pid_t pid, struct user_regs_struct *regs) {
	if (ptrace(PTRACE_GETREGS, pid, NULL, regs)<0) {
		perror("get_regs()");	
		return 0;
	}
	return 1;
}

char set_regs(pid_t pid, struct user_regs_struct *regs) {
	if (ptrace(PTRACE_SETREGS, pid, NULL, regs)<0) {
		perror("set_regs()");
		return 0;
	}
	return 1;
}

void show_regs(struct user_regs_struct *regs) {
	printf("-------------\n");
	printf("eax: 0x%x\n",regs->eax);
	printf("ebx: 0x%x\n",regs->ebx);
	printf("ecx: 0x%x\n",regs->ecx);
	printf("edx: 0x%x\n",regs->edx);
	printf("esi: 0x%x\n",regs->esi);
	printf("edi: 0x%x\n",regs->edi);
	printf("esp: 0x%x\n",regs->esp);
	printf("ebp: 0x%x\n",regs->ebp);
	printf("eip: 0x%x\n",regs->eip);
	printf("-------------\n");
}


char *read_mem(pid_t pid, unsigned long addr, size_t sz) {
	int i;
	
	char *buff = (char *)malloc(sz);
	bzero(buff,sz);	
	
	for (i=0; i < sz; i++) {
		buff[i] = (char)ptrace(PTRACE_PEEKTEXT, pid, addr++, 0);
	}
	
	return buff;
}


char write_mem(pid_t pid, unsigned long where, void *what, int sz) {
	void *p;

	for (p=what; p<what+sz; p+=4,where+=4) {
		//printf("write  0x%x: 0x%x\n",where,(*(unsigned long *)p));
		if (ptrace(PTRACE_POKETEXT, pid, where, (*(unsigned long *)p)) < 0 ) {
			printf("infection fails");
			return 0;
		}
	}

	return 1;
}



/*
void write_mem(pid_t pid, unsigned long addr, char *buff, size_t sz) {
	void *p = (void *)buff;

	for (i=0; i<sz; i+=4) {
		ptrace(PTRACE_POKETEXT, pid, addr++, p);
	}
}*/

void read_mem_to_file(pid_t pid, unsigned long baddr, unsigned long eaddr, FILE *fd) {
	unsigned long addr;
	int status = 0;
	int bytes = eaddr-baddr;
	
	for (addr = baddr; addr <= eaddr; addr++) {
		printf("%d%%   \r",(int)(status*100/bytes));
		status++;
		fflush(stdout);
		fputc((char)ptrace(PTRACE_PEEKTEXT, pid, addr, 0),fd);
	}	
}

void hookSignals(void (*fptr)(int)) {
    signal(SIGILL, fptr);
    signal(SIGBUS, fptr);
    signal(SIGFPE, fptr);
    signal(SIGINT, fptr);
    signal(SIGTERM, fptr);
} 

void step(pid_t pid) {
	
	
	if (ptrace(PTRACE_SINGLESTEP, pid, NULL, NULL)<0)
		perror("step()");
		
	//stat(pid);

}

void cont(pid_t pid) {
	//int s;
	if (ptrace(PTRACE_CONT, pid, NULL, NULL) < 0)
		perror("cont()");
		
	//wait(NULL);
	/*
	while (!WIFSTOPPED(s)) {
		waitpid(pid , &s , WNOHANG);
		sleep(5);
	}*/
}   




void getAddress(char dumpMode, int pid, unsigned long *baddr, unsigned long *eaddr) {
	char mapsfile[1024];
	char mapsline[1024];
	char addr[10];
	FILE *fd;

	sprintf(mapsfile,"/proc/%d/maps",pid);
	if ((fd = fopen(mapsfile,"r")) == NULL) {
		printf("invalid pid %d\n", pid);
		_exit(1);
	}
	if (fd < 0) {
		printf("incorrect pid\n");
		_exit(1);
	}

	while ((fgets(mapsline,1024,fd)) != NULL) {

		if (dumpMode == 1) {
			if (strstr(mapsline, "stack") != NULL) {
				memcpy(addr, mapsline, 8);
				*baddr = (unsigned long)strtoul(addr,NULL,16);
				memcpy(addr, mapsline+9, 8);
				*eaddr = (unsigned long)strtoul(addr,NULL,16);
				printf("%lx %lx\n",*baddr,*eaddr);
				//printf("stack at: 0x%x - 0x%x\n",baddr,eaddr);
				break;
			}

		} else if (dumpMode == 2) {
			if (strstr(mapsline, "heap") != NULL) {
				memcpy(addr, mapsline, 8);
				*baddr = (unsigned long)strtoul(addr,NULL,16);
				memcpy(addr, mapsline+9, 8);
				*eaddr = (unsigned long)strtoul(addr,NULL,16);
				//printf("heap at: 0x%x - 0x%x\n",baddr,eaddr);
				break;
			}
		}
		
	}

	fclose(fd);
}

void step_to_base(pid_t pid) {
	struct user_regs_struct regs;

	do {
		step(pid);
		waitpid(pid,NULL,0);
		if (!get_regs(pid, &regs)) {
			printf("problems getting regs!!\n");
			ptrace_detach(pid);
			_exit(1);
		}

	} while(regs.eip > 0x804fffff);
}



