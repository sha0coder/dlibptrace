import std.stdio;
import core.thread;
import core.memory;
import std.c.stdlib;

int clone(void function()fptr, void *stack) {
	asm {
		mov EAX, 0x78;		// __NR_clone
		mov EBX, 0xF11;		// (CLONE_VM | CLONE_FS | CLONE_FILES | CLONE_SIGHAND | SIGCHLD)
		mov ECX, [EBP+0x08]; // fptr
		mov EDX, [EBP+0x0c]; // strack
		int 3;
		int 0x80;
		test EAX, EAX;
		jne clone_end;
		call EDX;
		mov EAX,0x01;
		int 0x80;
		clone_end:;
	}
}

void test() {
	asm {
		int 3;
	}
}

void main() {
	GC.disable();
	void *stack = cast(void*)malloc(1024*3);
	stack = stack+1024*2;
	printf("0x%x\n",stack);
	clone(&test,stack);
	Thread.sleep(dur!("seconds")(5));
	writeln("hello");
}