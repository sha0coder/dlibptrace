import std.string;
import std.stdio; //writeln
import std.c.stdlib; // malloc()

struct user_regs_struct {
  long ebx;
  long ecx;
  long edx;
  long esi;
  long edi;
  long ebp;
  long eax;
  long xds;
  long xes;
  long xfs;
  long xgs;
  long orig_eax;
  long eip;
  long xcs;
  long eflags;
  long esp;
  long xss;
};


extern (C) {
	void cont_syscall(long pid);
	char wait_cont(long pid);
	int launch(char *file_name);
	char ptrace_attach(long pid);
	char ptrace_detach(long pid);
	char get_regs(long pid, ref user_regs_struct regs);
	char set_regs(long pid, ref user_regs_struct regs);
	char *read_mem(long pid, ulong addr, size_t sz);
	char write_mem(long pid, ulong where, void *what, int sz);
	//void read_mem_to_file(int pid, ulong baddr, ulong eaddr, FILE *fd);
	void hookSignals(void function(int) fptr);      // fixat com es fan aqui els callbacks, en c: void (*fptr)(int)
	void step(long pid);
	void cont(long pid);
	void getAddress(char dumpMode, int pid, ulong *baddr, ulong *eaddr);
	void step_to_base(long pid);
}


class Ptrace {
	private string name;
	private long pid;
	private bool regs_synched = false;
	public user_regs_struct regs;

	/*
	public void launch(string name) {
		this.name = name;
		if (launch(toStringz(name)) != 1)
			throw new Exception("can't execute.");
	}*/

	public void attach(long pid) {
		this.pid = pid;
		if (ptrace_attach(pid) != 1)
			throw new Exception("Can't attach");
	}

	public void detach() {
		ptrace_detach(pid);
	}

	public void getRegs() {
		get_regs(pid, regs);
		regs_synched = true;
		write("eip: ");
		writeln(regs.eip); // aqui no se usa el  ptrstruct->item  es ptrstruct.item, el ~ concatena  evitar usar printf ...
	}

	@property long eax() {
		if (!regs_synched)
			getRegs();

		return regs.eax;
	}

	@property long eip() {
		if (!regs_synched)
			getRegs();

		return regs.eip;
	}

}