import std.stdio;
import std.conv;
import dlibtrace;  // importar el binding, hay una tool para generarlo en base a un .h, en este caso lo he echo a mano

// Ptrace() no esta muy testeada ..

void main(string[] args) {

	auto pid = to!long(args[1]); //  !long es una template, es una funcion to!T(v) 

	auto p = new Ptrace(); // mejor usar auto y que el compiler deduzca el tipo
	p.attach(pid);
	writeln(p.eip); // esto es un @property get/set que sincroniza con getRegs() (cuando funcione bien ..)
	p.detach();


} // los descriptores se cierran solos y los mallocs se  hacen free solos :)