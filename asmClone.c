/*
	Ptrace infection POC
	mmap() + clone() from asm
	@sha0coder
*/


#include <stdio.h>
#include <sched.h>
#include <linux/sched.h>
#include <stdlib.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <linux/unistd.h>
#include <sys/mman.h>


#define TRAP "int $3\n"



void test(void *args) {
	sleep(1);
	//asm(TRAP);
	printf("virus: hi\n");
}




int iclone(void (*fptr)(void *)) {
	__asm__ __volatile__ (

		"mmap:							\n"
		"	movl $90, %eax			\n" // __NR_mmap
        "	xorl %ecx, %ecx			\n"
        "	pushl %ecx             	\n" // offset 0
        "	pushl $0xffffffff      	\n" // descriptor -1
        "	pushl $0x22            	\n" // privado     0x22  MAP_PRIVATE|MAP_ANONYMOUS
        "	pushl $3               	\n" // read|write  0x07
        "	pushl $0x800           	\n" // size 2048
        "	pushl %ecx             	\n" // null, mmap() will set the addr
        "	movl %esp, %ebx			\n" // ??
        "	int $0x80 					\n" // go!
        "	cmp $0xfffff000, %eax	\n" // check
        "	ja end						\n"     
        "	leal 0x800(%eax), %ecx	\n" // room for virus stack
        
		"clone:							\n"
		"	movl $0x78, %eax   			\n" // __NR_clone
		"	movl $0xf11, %ebx 			\n" // (CLONE_VM | CLONE_FS | CLONE_FILES | CLONE_SIGHAND | SIGCHLD)
		"	movl 0x8(%ebp), %edx 		\n" // fptr
		"	int $0x80 					\n" // go!
		
		"execution_control:				\n"
		"	testl %eax, %eax			\n"	// check return value 
		"	jne  end					\n"	// jump if is the host 
		
		"virus:							\n"
		"	call *%edx					\n"	// start virus function

		"barrier:"
		"	movl $1, %eax     			\n" // virus barrier
		"	int $0x80         			\n"
		
		"end:							\n"			
		"	leave						\n"
		"	ret							\n"
	);
}

unsigned int getESP() {
	__asm__ __volatile__ ("movl %esp, %eax");
}



void lock() {
	while (1) {
		printf("aaa\n");
		sleep(1);
		printf("bbb\n");
	}
}

int main() {

	/****************************/
	//void **stack = malloc(1024*3);
	
	//0xb7fdb800 0xb7735000
	//void **stack = mmap(NULL, 1024, PROT_READ|PROT_WRITE, MAP_PRIVATE|MAP_ANONYMOUS, -1, 0);
	//printf("0x%x\n",stack);
	//stack += 1024;
	printf("father stack's: 0x%x\n",getESP());
	iclone(test);
	lock();
	/****************************/




}
