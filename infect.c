/*
	by @sha0coder

	General purpose hot inector.
	1. backup stat
	2. inject the user asm code
	3. execute the code
	4. restore status keeping the inyected code resident


*/


#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <sys/user.h>
#include "libtrace.h"

/*
	0x00000000 ... [host code] ... [iclone code][host stat's backup][parasite code][parasite stack][edge][host stack] ... 0xffffffff
*/

#define PARASITE_STACK_SZ 1024
#define EDGE 100


#define FN_START(fn) (fn+4)
#define FN_END(fn) (fn##_end-3)
#define FN_SZ(fn) (FN_END(fn)-FN_START(fn))


pid_t pid;

void stop (int val) {
	printf("Signal %d received.\n", val);
	ptrace_detach(pid);
	printf("detached\n");
	_exit(1);
}

void parasite() {
	__asm__("int $3");
	__asm__("int $3");
	__asm__("int $3");
	__asm__("int $3"); 
}
void parasite_end() {}


void iclone() {
	__asm__ __volatile__ (
		"do_clone:						\n"
		"	int $0x80 					\n"
		
		"execution_control:				\n"
		"	testl %eax, %eax			\n"	
		"	jne  host					\n"	
		
		"spawn_parasite:				\n"
		"	call *%edx					\n"	

		"barrier:						\n"
		"	movl $1, %eax     			\n" 
		"	int $0x80         			\n"
		
		"host:							\n"	
		"	push %esi					\n"
		"	ret							\n"
		"recover:						\n"
		TRAP
	);
}
void iclone_end() {}




char infect(pid_t pid) {
	/*
		0x00000000 ... [host code] ... [iclone code][host stat's backup][parasite code][parasite stack][edge][host stack] ... 0xffffffff
	*/
	
	struct user_regs_struct regs;
	int iclone_eip;
	int parasite_eip;
	int parasite_esp;
	int regs_sz = sizeof(struct user_regs_struct);


	// get initial state
	get_regs(pid, &regs);
	
	
	// parasite destination
	parasite_esp = regs.esp-EDGE;
	parasite_eip = parasite_esp-PARASITE_STACK_SZ-FN_SZ(parasite);
	
	// iclone destination
	iclone_eip = parasite_eip-FN_SZ(iclone);
	
	// write iclone
	if (!write_mem(pid, iclone_eip, FN_START(iclone), FN_SZ(iclone))) {
		printf("infection fail\n");
		return 0;
	}
	
	// write parasite
	if (!write_mem(pid, parasite_eip, FN_START(parasite), FN_SZ(parasite))) {
		printf("infection fail\n");
		return 0;
	}

	// write host stat
	if (!write_mem(pid, parasite_esp-regs_sz, (void *)&regs, regs_sz)) {
		printf("infection fail\n");
		return 0;
	}

	
	// prepare new initial state
	regs.orig_eax = 0xffffffff;
	regs.esi = regs.eip;		// hosts eip
	regs.eip = iclone_eip;	
	regs.eax = 0x78;  // __NR_clone
	regs.ebx = 0xf11; // (CLONE_VM | CLONE_FS | CLONE_FILES | CLONE_SIGHAND | SIGCHLD)
	regs.ecx = parasite_esp;
	regs.edx = parasite_eip;
	set_regs(pid, &regs);

	printf("* infected!\n");
	return 1;
}



int main(int argc, char **argv) {
	struct user_regs_struct regs;

	if (argc != 2) {
		printf("usage %s [pid]\n",argv[0]);
		_exit(1);
	}

	pid = atol(argv[1]);

	hookSignals(stop);
	printf("Initial host stat: %c\n",getProcessStatus(pid));

	if (ptrace_attach(pid)) {
		printf("attached\n");

		step_to_base(pid);
		get_regs(pid,&regs);

		printf("eip: 0x%x\n",(unsigned int)regs.eip);
		infect(pid);

		ptrace_detach(pid);	
	}
	
	return 0;
}
