#include <sys/user.h>
#include <sys/types.h>

#define TRAP "int $3\n"

void cont_syscall(pid_t pid);
char wait_cont(pid_t pid);
int launch(const char *file_name);
char ptrace_attach(int pid);
char ptrace_detach(int pid);
char get_regs(pid_t pid, struct user_regs_struct *regs);
char set_regs(pid_t pid, struct user_regs_struct *regs);
char *read_mem(pid_t pid, unsigned long addr, size_t sz);
char write_mem(int pid, unsigned long where, void *what, int sz);
void read_mem_to_file(int pid, unsigned long baddr, unsigned long eaddr, FILE *fd);
void hookSignals(void (*fptr)(int));
void step(pid_t pid);
void cont(pid_t pid);
void getAddress(char dumpMode, int pid, unsigned long *baddr, unsigned long *eaddr);
void step_to_base(pid_t);


