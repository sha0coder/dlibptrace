CC_ANDROID = gcc-arm-linux-gnueabi
CC_ANDROID = arm-linux-gnueabi-gcc
CC_PC = gcc

CC = ${CC_PC}
CFLAGS = -O2 -g  -static


all: infect


infect: libtrace.o
	dmd dummy.d
	$(CC) $(CFLAGS) infect.c libtrace.o -o infect


libtrace.o: clean
	$(CC) $(CFLAGS) -c libtrace.c


clean:
	rm -f infect dummy  *~ *.o




